# Plant monitor
Monitor a plant's envoriment remotely with a microcontroller and Grafana.

## Hardware:
![img](images/hardware.jpg)

A custom pcb brings together a photoresistor, a soil humidity sensor and a HTU21 ambient temperature and relative humidity sensor to make monitoring a plant's envoriment easy.

## Firmware:
Built with the rust ESP32-C3 HAL and embassy, it makes for a robust and versatile solution.

## Cloud:
![img](images/dashboard.png)

A docker stack compose makes deploying grafana, prometheus and pushgateway all at once easy. Pushgateway means it's easy to collect data from anywhere on the world, and grafana makes it easy to visualize. On top of that, alerts can be used to warn about dry soil or extreme temperatures.
 
## Notes
 - Due to this [merge request](https://github.com/rust-lang/rust/pull/114499), atomics broke on RiscV. Make sure you're using the `nightly` version specified on `rust-toolchain.toml` 
 - `smoltcp` as a secondary dependency will crash if your router serves more DNS servers than it is configured to use. See [smoltcp configuration](https://github.com/smoltcp-rs/smoltcp#configuration)
