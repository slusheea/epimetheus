#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use core::fmt::Write;

use esp32c3_hal as hal;
use hal::{
    adc::{AdcConfig, Attenuation, ADC, ADC1},
    clock::{ClockControl, Clocks},
    embassy,
    gpio::IO,
    i2c::I2C,
    interrupt,
    interrupt::Priority,
    peripherals::{Interrupt, Peripherals, APB_SARADC, GPIO, I2C0, IO_MUX},
    prelude::*,
    timer::TimerGroup,
    Delay, Rng,
};

use esp_wifi::{
    initialize,
    wifi::{WifiController, WifiDevice, WifiEvent, WifiStaDevice, WifiState},
    EspWifiInitFor,
};

use esp_backtrace as _;
use esp_println::println;

use embassy_executor::Spawner;
use embassy_net::{dns::DnsQueryType, tcp::TcpSocket, Config, Stack, StackResources};
use embassy_time::{Duration, Timer};

use embedded_svc::wifi::{ClientConfiguration, Configuration, Wifi};

use static_cell::make_static;

use gy_21::Gy21;

use heapless::String;

const SSID: &str = env!("SSID");
const PASSWORD: &str = env!("PASSWORD");

// soil humidity sensor and photoresistor calibration values
const SHUM_MIN: f32 = 4095.0;
const SHUM_MAX: f32 = 2230.0;
const LIGHT_MIN: f32 = 1675.0;
const LIGHT_MAX: f32 = 4095.0;
const NEW_MIN: f32 = 0.0;
const NEW_MAX: f32 = 100.0;

const PUSH_SERVER: &str = "pushgateway.slushee.gay";
const PUSH_PORT: u16 = 9091;
const JOB: &str = "plant_monitor";
const REQUEST_SIZE: usize = 254;
const DATA_SIZE: usize = 32;

#[main]
async fn main(spawner: Spawner) -> ! {
    let mut peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::max(system.clock_control).freeze();
    let timer = hal::systimer::SystemTimer::new(peripherals.SYSTIMER).alarm0;

    let mut rng = Rng::new(&mut peripherals.RNG);
    let seed: u64 = rng.random().into();

    let init = initialize(
        EspWifiInitFor::Wifi,
        timer,
        Rng::new(peripherals.RNG),
        system.radio_clock_control,
        &clocks,
    )
    .unwrap();

    let wifi = peripherals.WIFI;
    let (wifi_interface, controller) =
        esp_wifi::wifi::new_with_mode(&init, wifi, WifiStaDevice).unwrap();

    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    embassy::init(&clocks, timer_group0.timer0);

    let config = Config::dhcpv4(Default::default());

    // Init network stack
    let stack = &*make_static!(Stack::new(
        wifi_interface,
        config,
        make_static!(StackResources::<3>::new()),
        seed
    ));

    // Execute embassy tasks
    spawner.spawn(connection(controller)).ok();
    spawner.spawn(net_task(stack)).ok();
    spawner
        .spawn(task(
            stack,
            peripherals.GPIO,
            peripherals.IO_MUX,
            peripherals.APB_SARADC,
            peripherals.I2C0,
            clocks,
        ))
        .ok();
}

#[embassy_executor::task]
async fn connection(mut controller: WifiController<'static>) {
    loop {
        if esp_wifi::wifi::get_wifi_state() == WifiState::StaConnected {
            // wait until we're no longer connected
            controller.wait_for_event(WifiEvent::StaDisconnected).await;
            Timer::after(Duration::from_millis(5000)).await
        }

        if !matches!(controller.is_started(), Ok(true)) {
            println!("Attempting to connect to {}...", SSID);
            let client_config = Configuration::Client(ClientConfiguration {
                ssid: SSID.into(),
                password: PASSWORD.into(),
                ..Default::default()
            });
            controller.set_configuration(&client_config).unwrap();
            controller.start().await.unwrap();
        }

        match controller.connect().await {
            Ok(_) => println!("Connected.\n"),
            Err(e) => {
                println!("Failed to connect to wifi: {e:?}");
                Timer::after(Duration::from_millis(5000)).await
            }
        }
    }
}

#[embassy_executor::task]
async fn net_task(stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>) {
    stack.run().await
}

#[embassy_executor::task]
async fn task(
    net_stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>,
    gpio: GPIO,
    io_mux: IO_MUX,
    apb_saradc: APB_SARADC,
    i2c0: I2C0,
    clocks: Clocks<'static>,
) {
    let mut rx_buffer = [0; 4096];
    let mut tx_buffer = [0; 4096];

    // Set up ADC pins
    let io = IO::new(gpio, io_mux);
    let analog = apb_saradc.split();
    let mut adc_config = AdcConfig::new();
    let mut soil_humidity_sensor =
        adc_config.enable_pin(io.pins.gpio2.into_analog(), Attenuation::Attenuation11dB);
    let mut photoresistor =
        adc_config.enable_pin(io.pins.gpio3.into_analog(), Attenuation::Attenuation11dB);
    let mut adc = ADC::<ADC1>::adc(analog.adc1, adc_config).unwrap();

    let delay = Delay::new(&clocks);
    let i2c = I2C::new(
        i2c0,
        io.pins.gpio6,
        io.pins.gpio7,
        40u32.kHz(), // Higher speeds lead to incorrect temperature measurements
        &clocks,
    );
    interrupt::enable(Interrupt::I2C_EXT0, Priority::Priority1).unwrap();
    let mut gy_21 = Gy21::new(i2c, delay);

    loop {
        if net_stack.is_link_up() {
            break;
        }
        Timer::after(Duration::from_millis(500)).await;
    }

    println!("Waiting to get IP address...");
    loop {
        if let Some(config) = net_stack.config_v4() {
            println!("Got IP: {}\n", config.address);
            break;
        }
        Timer::after(Duration::from_millis(1000)).await;
    }

    loop {
        Timer::after(Duration::from_millis(1_000)).await;

        let mut socket = TcpSocket::new(net_stack, &mut rx_buffer, &mut tx_buffer);

        socket.set_timeout(Some(embassy_time::Duration::from_secs(10)));

        println!("Querrying DNS servers for {}...", PUSH_SERVER);
        let remote_endpoint = net_stack
            .dns_query(PUSH_SERVER, DnsQueryType::A)
            .await
            .unwrap()[0];

        println!(
            "First A record is {}\nAttempting to connect...",
            &remote_endpoint
        );
        let r = socket.connect((remote_endpoint, PUSH_PORT)).await;
        if let Err(e) = r {
            println!("Connection error: {:?}", e);
            continue;
        }
        println!("Connection successful.");

        let mut buf = [0; 1024];
        'main: loop {
            let shum = normalize(
                nb::block!(adc.read(&mut soil_humidity_sensor)).unwrap(),
                (SHUM_MIN, SHUM_MAX),
                (NEW_MIN, NEW_MAX),
            );
            let light = normalize(
                nb::block!(adc.read(&mut photoresistor)).unwrap(),
                (LIGHT_MIN, LIGHT_MAX),
                (NEW_MIN, NEW_MAX),
            );
            let temp = gy_21.temperature().unwrap_or(0.0);
            let rhum = gy_21.humidity().unwrap_or(0.0);
            let dewt = gy_21.dew_point_temp().unwrap_or(0.0);

            let readings: [(&str, f32); 5] = [
                ("shum", shum),
                ("light", light),
                ("temp", temp),
                ("rhum", rhum),
                ("dewt", dewt),
            ];

            use embedded_io_async::Write;
            for (label, value) in readings {
                let mut request: String<REQUEST_SIZE> = String::new();
                let mut data: String<DATA_SIZE> = String::new();
                write!(&mut data, "{} {}", label, value).unwrap();
                write!(
                    &mut request,
                    "POST /metrics/job/{} HTTP/1.1\nHost: {}:{}\nContent-Length: {}\nContent-Type: application/x-www-form-urlencoded\n\n{}\n\n",
                    JOB, remote_endpoint, PUSH_PORT, (data.len()+1), data
                )
                .unwrap();
                println!("{}", request);

                let r = socket.write_all(request.as_bytes()).await;
                if let Err(e) = r {
                    println!("Write error: {:?}", e);
                    break 'main;
                }
                match socket.read(&mut buf).await {
                    Ok(0) => {
                        break 'main;
                    }
                    Err(e) => {
                        println!("Read error: {:?}", e);
                        break 'main;
                    }
                    Ok(n) => println!("{}", core::str::from_utf8(&buf[..n]).unwrap()),
                };
            }
            Timer::after(Duration::from_secs(1800)).await;
        }
        Timer::after(Duration::from_millis(3000)).await;
    }
}

fn normalize(input: u16, (old_min, old_max): (f32, f32), (new_min, new_max): (f32, f32)) -> f32 {
    let old_range = old_max - old_min;
    let new_range = new_max - new_min;
    ((input as f32 - old_min) / old_range) * new_range + new_min
}
